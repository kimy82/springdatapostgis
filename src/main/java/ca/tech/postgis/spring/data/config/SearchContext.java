package ca.tech.postgis.spring.data.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "org.springframework.data.solr.showcase.product", entityManagerFactoryRef = "entityManager", transactionManagerRef = "transaction")
public class SearchContext {

   @Bean(name = "entityManagerFactory")
   public LocalContainerEntityManagerFactoryBean entityManagerFactoryBolsa() {
      LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
      em.setDataSource(dataSource());
      em.setPackagesToScan(new String[] { "ca.tech.postgis.spring.data.product.model" });

      JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
      em.setJpaVendorAdapter(vendorAdapter);
      em.setJpaProperties(additionalProperties());

      return em;
   }

   @Bean(name = "transactionManager")
   public PlatformTransactionManager transactionManagerBolsa() {
      JpaTransactionManager transactionManager = new JpaTransactionManager();
      transactionManager.setEntityManagerFactory(entityManagerFactoryBolsa().getObject());

      return transactionManager;
   }

   @Bean(name = "dataSource", autowire = Autowire.BY_NAME)
   public DataSource dataSource() {
      DriverManagerDataSource dataSource = new DriverManagerDataSource();
      dataSource.setDriverClassName("org.postgresql.Driver");
      dataSource.setUrl("jdbc:postgresql://localhost:5432/postgis");
      dataSource.setUsername("postgres");
      dataSource.setPassword("admin");
      return dataSource;
   }
   
   Properties additionalProperties() {
      Properties properties = new Properties();
      properties.setProperty("hibernate.hbm2ddl.auto","update");
      properties.setProperty("hibernate.dialect",
            "org.hibernate.spatial.dialect.postgis.PostgisDialect");
      properties
            .setProperty("hibernate.show_sql", "false");
      properties.setProperty("hibernate.format_sql",
            "false");
      return properties;
   }
}
