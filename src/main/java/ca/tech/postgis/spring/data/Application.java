package ca.tech.postgis.spring.data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ca.tech.postgis.spring.data.config.SearchContext;


@Configuration
@ComponentScan
@EnableAutoConfiguration
@Import({ SearchContext.class })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
