package ca.tech.postgis.spring.data.product;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ca.tech.postgis.spring.data.product.model.Product;

public interface ProductRepository extends CrudRepository<Product, String> {

   @Query(value="SELECT id, name, geom FROM line WHERE ST_DWithin(geom, ST_GeomFromText('POINT(49.18814 -93.88541)', 26918),0.9) LIMIT 20", nativeQuery = true)
	List<Product> findProductsWithin();
   
   @Query(value="SELECT count(*) FROM line WHERE ST_DWithin(geom, ST_GeomFromText('POINT(12.18814 -93.88541)', 26918),0.9)", nativeQuery = true)
   Long countProductsWithin();
}
