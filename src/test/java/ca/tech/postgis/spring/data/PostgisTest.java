package ca.tech.postgis.spring.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import ca.tech.postgis.spring.data.product.ProductRepository;
import ca.tech.postgis.spring.data.product.model.Product;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = { Application.class })
public class PostgisTest {

   
   @Resource
   private ProductRepository productRepository;

   private Long index = 0L;

   @Before
   public void setUp() {
    //  this.productRepository.deleteAll();
      this.index = this.productRepository.count();
    //  addBatch(500000L);
   }
   
   @Test
   public void ProductPostGisGeoTest() {
      Calendar cal = Calendar.getInstance();
      Long startMil = cal.getTimeInMillis();
      System.out.println(this.productRepository.findProductsWithin().size());
      cal = Calendar.getInstance();
      Long endMil = cal.getTimeInMillis();
      System.out.println("Time spent on " + index + ": " + (endMil - startMil));
      
      cal = Calendar.getInstance();
      startMil = cal.getTimeInMillis();
      System.out.println(this.productRepository.countProductsWithin());
      cal = Calendar.getInstance();
      endMil = cal.getTimeInMillis();
      System.out.println("Time spent on " + index + ": " + (endMil - startMil));
   }
   
   private void addBatch(Long numberOfInserts) {

      List<Product> list = new ArrayList<Product>();
      Long lastIndex = index;
      for (index = lastIndex; index < (numberOfInserts + lastIndex); index++) {
         Product product = new Product();
         product.setId(index);
        
         double latitude = 45.18814;
         latitude = (latitude + index) % 90;
         Coordinate coord = new Coordinate(latitude, -93.88541);
         PrecisionModel pm =new PrecisionModel();
         Point point = new Point(coord,pm,26918);
         
         WKTReader fromText = new WKTReader();
         Geometry geom = null;
         try {
             geom = fromText.read("POINT("+latitude+" -93.88541)");
             geom.setSRID(26918);
         } catch (ParseException e) {
             throw new RuntimeException("Not a WKT string:");
         }
         if (!geom.getGeometryType().equals("Point")) {
             throw new RuntimeException("Geometry must be a point. Got a " + geom.getGeometryType());
         }
         
         product.setGeom((Point)geom);
         product.setName("name" + index);
         list.add(product);
         if (list.size() > 100000) {
            saveList(list);
         }
      }

      saveList(list);
   }

   private void saveList(List<Product> list) {
      this.productRepository.save(list);
      System.out.println("Saving>> " + index);
      list.clear();
   }
}
