#SPRING DATA POSTGIS

##GOAL.

Test speed of postgres/postgis searching within a circle.

##Set up

Refer to [postgis install](http://postgis.net/install/) to install postgis

**Table creation**

CREATE TABLE mytable ( 
  id SERIAL PRIMARY KEY,
  geom GEOMETRY(Point, 26910),
  name VARCHAR(128)
); 


**Index creation**
CREATE INDEX products_index ON line USING GIST (geom);


**SQL utils** 

To make sure your postgis is really using the index run:

/////////////////////////////////////////////////////////////////////////////////
postgis=# EXPLAIN ANALYZE
postgis-# SELECT id, name, geom FROM line WHERE ST_DWithin(geom, ST_GeomFromTex
('POINT(45.18814 54)', 27910),1);

 Index Scan using products_index on line  (cost=0.29..8.56 rows=1 width=50) (ac
ual time=0.588..87.282 rows=11220 loops=1)
   Index Cond: (geom && '0103000020066D00000100000005000000E388B5F8141846400000
00000804A40E388B5F8141846400000000000804B40E388B5F8141847400000000000804B40E388
5F8141847400000000000804A40E388B5F8141846400000000000804A40'::geometry)
   Filter: (('0101000020066D0000E388B5F8149846400000000000004B40'::geometry &&
t_expand(geom, '1'::double precision)) AND _st_dwithin(geom, '0101000020066D000
E388B5F8149846400000000000004B40'::geometry, '1'::double precision))
   Rows Removed by Filter: 5610
 Planning time: 0.366 ms
 Execution time: 98.203 ms
(6 rows)
///////////////////////////////////////////////////////////////////////////////////


##RESULTS

This results are only fetching the first 20 rows. And configuration as the default one (check postgres.conf) for more info. Some of the most 
relevant for this study are:

*shared_buffers = 128Mb (Caching data)
*work_mem = 4mb

You can check on Database doing _show  shared_buffers_ or _show  work_mem_

|num rows	| time(ms)/number of results	|
|-----------|:-----------------------------:|
|500,000	|   300/5556					|
|1,000,000  |   385/11112					|
|1,500,000	|   674/16667					|
|2,000,000  |   614/22223					|
|2,500,000	|   717/27778					|
|3,000,000  |   871/33334					|
|3,500,000  |   891/38889					|
|4,000,000  |  	1056/44445					|
|4,500,000  |  	1161/50000					|
|5,000,000  |  	2716/55556					|
|5,500,000  |  	3838/61111					|
|6,000,000  |  	2530/66667					|
|6,500,000  |  	2795/72223					|
|7,000,000  |  	7860/77778					|
|7,500,000  |  	7778/83334					|
|8,000,000  |  	4909/88889					|
|8,500,000  |  	7282/94445					|
|9,000,000  |  	7913/100000					|
|9,500,000  |  	5487/105556					|
|10,000,000 |  	4917/111111					|
|10,500,000 |  	8082/116667					|
|11,000,000 |	7209/122222					|
|11,500,000 |	6369/127778					|
|12,000,000 |	6690/133334					|

*shared_buffers = 512Mb (Caching data)
*work_mem = 4mb
*Fist time is running a random query direct to the DB, using psql.
*Second time is using spring data layer with a fixed geospatial query.

|numb rows  | time(ms)/number of results	|
|-----------|:-----------------------------:|
|500,000    |   300/5556					|
|1,000,000  |   40-110/11112				|
|1,500,000	|   63-140/16667				|
|2,000,000  |   85-184/22223				|
|2,500,000	|   120-222/27778				|
|3,000,000  |   133-231/33334				|
|3,500,000  |   155-302/38889				|
|4,000,000  |  	430-366/44445				|
|4,500,000  |  	600-350/50000				|
|5,000,000  |  	450-354/55556				|
|5,500,000  |  	1900-4768/61111				|
|6,000,000  |  	2100-2060/66667				|
|6,500,000  |  	2758-2920/72223				|
|7,000,000  |  	2668-2293/77778				|
|7,500,000  |  	3357-3461/83334				|
|8,000,000  |  	4790-5470/88889				|
|8,500,000  |  	4930-4450/94445				|
|9,000,000  |  	5020-7755/100000			|
|9,500,000  |  	4450-4952/105556			|
|10,000,000 |  	7208-5080/111111			|
|10,500,000 |  	8310-6252/116667			|
|11,000,000 |  	8852-7241/122222			|
|11,500,000 |  	10609-6307/127778			|
|12,000,000 |  	12492-6798/133334			|

**DB doesn't peform correctly when deleting items. There is a need to drop and recreate the index.
